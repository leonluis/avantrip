import time
import itertools
import sys
import re
import concurrent.futures
import requests
import prometheus_client
from pprint import pprint
from memory_profiler import profile, memory_usage
from .helpers.middleware import setup_metrics
from voluptuous import Schema, Required, All, Match, Datetime, Length, ALLOW_EXTRA
from flask import Flask, render_template, url_for, request, Response, jsonify

app = Flask("Avantrip")

# Prometheus Metrics setup
setup_metrics(app)
CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')


# Avantrip API base URL
API_BASE_URL = 'https://api.avantrip.com/flights-api/v1/flights/roundtrips'

# Avantrip API client key
CLIENT_KEY = '2ddffe14f22f63d33a7f'

""" 
Accepted Avantrip API parameters on /roundtrips endpoint, use this schema to validate args
"""
schema = Schema({
    Required('clientKey'): All(str),
    Required('from'): All(Match((r'^[A-Z]*$'), msg='required, only string iata codes accepted'), Length(max=3)),
    Required('to'): All(Match((r'^[A-Z]*$'), msg='required, only string iata codes accepted'), Length(max=3)),
    Required('departureDate'): Datetime(format='%Y-%m-%d', msg='required, invalid date format, YYYY-MM-DD required'),
    Required('returningDate'): Datetime(format='%Y-%m-%d', msg='required, invalid date format, YYYY-MM-DD required'),
    Required('adults'): All(Match((r'^[0-9]'), msg='required, only integer values from 1-9 accepted'), Length(max=1)),
    Required('cabinClass'): All(Match((r'\b(?:economy|business)\b'), msg='required, accepted cabin classes are economy - business'))
}, extra=ALLOW_EXTRA)


@app.route('/api/v1/flights/roundtrips')
def get_roundtrips():
    """
    Rountrip endpoint, results can be ordered ascending, descending and limited.
    Consumes https://api.avantrip.com/flights-api/v1/flights/roundtrips
    Example: using 'sort=asc&limit=5' will return 5 of the cheapest flights.
    """

    ''' 
    Parameters to query the Avantrip API 
    Public API has these exposed but children and infants are not required
    "cabinClass=first" seems to fail so it was excluded from this api
    '''
    params = {
        'clientKey': CLIENT_KEY,
        'from': request.args.get('from'),
        'to': request.args.get('to'),
        'departureDate': request.args.get('departureDate'),
        'returningDate': request.args.get('returningDate'),
        'adults': request.args.get('adults'),
        'children': request.args.get('children'),
        'infants': request.args.get('infants'),
        'cabinClass': request.args.get('cabinClass')
    }

    try:
        schema(params)  # Parameters validation
    except Exception as e:
        return str(e)

    ''' This fuctions does the API call using the pareters passed to the endpoint'''
    def queryAvantrip(API_BASE_URL, params):
        r = requests.get(
            url=f"{API_BASE_URL}",
            params=params, verify=False, headers={'Connection':'close'})
        return r

    ''' Executes the "queryAvantrip" '''
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(queryAvantrip, API_BASE_URL, params)
        r = future.result()

    ''' This function sorts flights asc/desc depending on request params'''
    def sortResponse(response, sort=None, limit=None):
        if (sort != 'asc') and (sort != 'desc'):
            return response.json()['clusters'][:limit]
        elif sort == 'asc':
            return sorted(response.json()['clusters'], key=lambda k: int(
                k['totalPrice']), reverse=False)[:limit]
        elif sort == 'desc':
            return sorted(response.json()['clusters'], key=lambda k: int(
                k['totalPrice']), reverse=True)[:limit]
    
    

    if 'clusters' in r.json():
        
        limit = request.args.get('limit')

        # checks if limit parameter exists and contains a number
        if limit and re.search(r'^[0-9]*$', request.args.get('limit')):
            limit = int(request.args.get('limit'))
        else:
            limit = 10                      
        api_response = sortResponse(r, request.args.get(
            'sort'), limit=int(limit))
        used_memory = memory_usage((sortResponse, (r, request.args.get(
            'sort')), {'limit' : int(limit)}))
    else:
        api_response = r.json()

    payload = {
        # excludes the api key from the parameters so is not exposed
        'params': {k: v for (k, v) in params.items() if k != 'clientKey'},
        'url': r.url,
        'metrics': {
            'elapsed_time': r.elapsed.total_seconds(),
            'used_memory': [used_memory]
        },
        'avantrip': {
            'flights': api_response
        }
    }
    
    response = jsonify(payload)
    return response


@app.route('/metrics')
def metrics():
    return Response(prometheus_client.generate_latest(), mimetype=CONTENT_TYPE_LATEST)
