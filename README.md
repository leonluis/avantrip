# Avantrip Test - Deploy API to Amazon ECS using CloudFormation and GitlabCI

Creates and run a docker container in Amazon ECS using CloudFormation and aws cli.

With this repo you will be able to:

    - Containerize a simple APP to consume [Avantrip's public API](https://api.avantrip.com/flights-api/v1/flights/roundtrips?clientKey=2ddffe14f22f63d33a7f&from=BUE&to=MIA&departureDate=2020-07-17&adults=1&children=0&infants=0&cabinClass=economy&returningDate=2020-07-27)
    - Using Gitlab CI/CD:
        - Create an Amazon ECR repository
        - Build a Docker image and push it to Amazon ECR
        - Deploy a CloudFormation stack to create a VPC, Subnets, InternetGateway and other networking appliances
        - Create IAM roles and security groups
        - Spin up an ECS Cluster, Loadbalancer & Listener
        - Run a Service with a Task Definition that runs the APP


## Deployment

### First, clone this repository to your machine

```
git clone https://gitlab.com/leonluis/avantrip.git
```

### Run the application locally using Docker

```
cd avantrip
docker build -t avantrip .
docker run -d -p 5000:5000 avantrip:latest
visit http://localhost:5000/metrics to see the initial metrics collection
visit http://localhost:5000/api/v1/flights/roundtrips to access the API
```

### Run the application locally using docker-compose

This stack spins up the Avantrip API container along with Prometheus and Grafana

```
cd avantrip
docker-compose --project-name Avantrip up -d
visit http://localhost:5000/metrics to see the initial metrics collection
visit http://localhost:5000/api/v1/flights/roundtrips to access the API
```

### Gitlab CI/CD automatic deploy

Using Gitlab CI/CD and Cloudformation templates, this respository is configured to automatically deploy the application
to AWS ECS. It uses Docker in Docker runners to build, push and execute aws-cli commands in the various stages of the .gitlab-ci.yml configuration file.

Here's what you need to complete in order to get it working for your environment:

    1. Get an AWS Account
	2. Create an IAM User different from root with admin/ECS privileges
	3. Generate an Access key/Secret Key for that user
    4. Fork this repository
	5. Configure the following variables in your Gitlab Project under Settings > CI/CD > Secret Variables using your own values:
		- AWS_ACCESS_KEY_ID
		- AWS_SECRET_ACCESS_KEY
		- AWS_DEFAULT_REGION
		- AWS_ACCOUNT_ID
		** remember to mask sensitive data to prevent it appearing in pipelines/jobs logs

        
        
    6. For convenience, given the interview challenge, the pipeline is configured so the stages won't trigger unless there is a specific tag in the commit
        message. 
        
        - Build Stage: include "{PushToECR}" without quotes. This will build the docker image and push it to ECR.
        - Deploy Stage: include "{DeployToECS}" without quotes. Including this tag will trigger the Cloudformation stacks to be executed against AWS.

    7. Once the pipeline completes, access your AWS account, under Cloudformation -> Stacks -> avantrip-service -> Output tab you will find the Output key "ApiEndpoint" that contains the URL where you can access the application. Ie: http://ecs-avantrip-service-649937122.us-east-1.elb.amazonaws.com/api/v1/flights/roundtrips

    
Variables reference:

![alt text](img/variables.png "Secret Variables")

ApiEndpoint Key reference:

![alt text](img/endpoint.png "ApiEndpoint")

### Cloudformation stacks

vpc.yml

    Creates AWS resources in order to allow the aplication to run in a controlled networking environment. It creates 2 public subnets on different
    availability zones, an Internet Gateway and routes that allow incoming traffic from external networks, while configuring routes and subnet asociations to the
    fresh started VPC.

        - AWS::EC2::VPC
        - AWS::EC2::Subnet
        - AWS::EC2::InternetGateway
        - AWS::EC2::VPCGatewayAttachment
        - AWS::EC2::RouteTable
        - AWS::EC2::SubnetRouteTableAssociation
        - AWS::EC2::Route

iam.yml

    It configures the TaskExecutionRolePolicy that allows ECS tasks to download docker images from ECR and to log events to Cloudwatch groups.
        
        - AWS::IAM::Role

ecr.yml

    Creates an AWS ECR Repository, so we can push/pull our docker image.

        - AWS::ECR::Repository

cluster.yml

    This template spins up an ECS Cluster where our applications is going to run on. For that to be achieved, it first creates a Load Balancer, its listener,
    a security groups that allow traffic from the internet reach the container exposed ports and a Cloudwatch group to log events.

        - AWS::ECS::Cluster
        - AWS::ElasticLoadBalancingV2::LoadBalancer
        - AWS::ElasticLoadBalancingV2::Listener
        - AWS::EC2::SecurityGroup
        - AWS::ElasticLoadBalancingV2::TargetGroup
        - AWS::Logs::LogGroup
        - AWS::EC2::SecurityGroup

api.yml

    The one and only, the template that declares the ECS Service that contains the TaskDefinition from where our AvantripAPI container is run. It also
    creates a Target group to perform basic health checks on one of the application endpoints.

        - AWS::ECS::TaskDefinition
        - AWS::ECS::Service
        - AWS::ElasticLoadBalancingV2::TargetGroup
        - AWS::ElasticLoadBalancingV2::ListenerRule

## Application usage

### Examples

[Avantrip's public API](https://api.avantrip.com/flights-api/v1/flights/roundtrips?clientKey=2ddffe14f22f63d33a7f&from=BUE&to=MIA&departureDate=2020-07-17&adults=1&children=0&infants=0&cabinClass=economy&returningDate=2020-07-27)

After running the application using your favorite deploy method, the following endpoints are available:

```
/metrics
/api/v1/flights/roundtrips 
```

The API was created in order to consume the public endpoint and retrieve the cheapest flights based on:
    - price
    - sorting [asc/dec]
    - limit [default=10]

Accepted parameters:

    - from (IATA codes for airports, ie: BUE)
    - to (IATA codes for airports, ie: MIA)
    - departureDate (YYYY-MM-DD format)
    - returningDate (YYYY-MM-DD format)
    - adults (1-9 passengers combined)
    - children (1-9 passengers combined)
    - infants (1-9 passengers combined)
    - cabinClass (economy, business, first*)
    - sort (asc/desc)
    - limit

    * not working at the moment for the original API consumed

Example API query:

```
# get
curl http://127.0.0.1:5555/api/v1/flights/roundtrips?from=BUE&to=MIA&departureDate=2020-07-17&adults=1&cabinClass=business&returningDate=2020-07-27&sort=asc&limit=1
```

### Metrics

/metrics endpoint has Prometheus ready metrics available for comsumption, they meassure different performance indicators like latency, requests time and others.

When making a GET request to the main /api/v1/flights/roundtrips endpoint, you will get a simplified memory OS memory consumption summary provided by the backend in the JSON response.

If you used the docker-compose aproach, prometheus and grafana containers are going to be available on the following paths:

```
visit http://localhost:9090/graph/ to check prometheus
visit http://localhost:3000/ to start using grafana
```
Prometheus example query: request_latency_seconds_bucket

![alt text](img/prometheus.png "Prometheus Graph")

### Stress Test

Using Apache Benchmark

```
Endoint /metrics
1000 requests with 100 concurrent connections

ab -n 1000 -c 100 http://hostname:5000/metrics
```

/metrics results

```
Endoint /metrics

Completed 100 requests
Completed 200 requests
Completed 300 requests
Completed 400 requests
Completed 500 requests
Completed 600 requests
Completed 700 requests
Completed 800 requests
Completed 900 requests
Completed 1000 requests
Finished 1000 requests


Server Software:        gunicorn/20.0.4
Server Hostname:        138.197.71.254
Server Port:            5000

Document Path:          /metrics
Document Length:        2598 bytes

Concurrency Level:      100
Time taken for tests:   2.304 seconds
Complete requests:      1000
Failed requests:        675
   (Connect: 0, Receive: 0, Length: 675, Exceptions: 0)
Total transferred:      2794979 bytes
HTML transferred:       2601979 bytes
Requests per second:    434.02 [#/sec] (mean)
Time per request:       230.403 [ms] (mean)
Time per request:       2.304 [ms] (mean, across all concurrent requests)
Transfer rate:          1184.65 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   1.4      0      14
Processing:     9  220  39.2    226     284
Waiting:        9  220  39.2    226     283
Total:         13  221  38.5    227     288

Percentage of the requests served within a certain time (ms)
  50%    227
  66%    232
  75%    237
  80%    240
  90%    248
  95%    268
  98%    277
  99%    279
 100%    288 (longest request)
```

/api/v1/flights/roundtrips results

```
ab -n 100 -c 10 http://hostname:5000/api/v1/flights/roundtrips?from=BUE&to=MIA&departureDate=2020-07-17&adults=1&lol=1&children=1&infants=0&cabinClass=economy&returningDate=2020-07-27
```

```Server Software:        gunicorn/20.0.4
Server Hostname:        138.197.71.254
Server Port:            5000

Document Path:          /api/v1/flights/roundtrips?from=BUE&to=MIA&departureDate=2020-07-17&adults=1&lol=1&children=1&infants=0&cabinClass=economy&returningDate=2020-07-27
Document Length:        11832 bytes

Concurrency Level:      10
Time taken for tests:   120.723 seconds
Complete requests:      100
Failed requests:        98
   (Connect: 0, Receive: 0, Length: 98, Exceptions: 0)
Total transferred:      1199306 bytes
HTML transferred:       1183806 bytes
Requests per second:    0.83 [#/sec] (mean)
Time per request:       12072.291 [ms] (mean)
Time per request:       1207.229 [ms] (mean, across all concurrent requests)
Transfer rate:          9.70 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   1.1      1       7
Processing:  7689 10916 4562.9   8173   29358
Waiting:     7689 10916 4562.9   8172   29357
Total:       7690 10917 4563.1   8173   29358

Percentage of the requests served within a certain time (ms)
  50%   8173
  66%  12660
  75%  12887
  80%  13026
  90%  17177
  95%  19006
  98%  29284
  99%  29358
 100%  29358 (longest request)
```

## Technical Challenge questions (Spanish)

Q. **Qué cosas tendrías en cuenta que hay que realizar en un evento de concurrencia
masiva? ¿Por qué?**

A. Servir el contenido estático desde un CDN (Cloudfront, S3) siempre es recomendado, sin embargo, una de las mejores formas de prepararse para incrementos significativos en el flujo de trafico es configurar grupos de autoscaling basados en uso de CPU y Memoria. Esto permite utilizar instancias de menor costo y adaptar la infraestructura en base a la demanda, en vez de desperdiciar dinero y recursos al mantener una instancia de mayor tamaño ejecutándose a lo largo del tiempo.

Q. **Hay un error en el ambiente productivo, 50% de los requests retornan un error. En una estructura de alta disponibilidad, ¿qué puede estar sucediendo y cómo diagnosticarías estos problemas? ¿Por qué?**

A. El primer paso es identificar el error mostrado en el response del request, de esta forma tenemos un indicio de qué problema podríamos estar enfrentando. Asumiendo que tenemos una infraestructura con un balanceador clásico, se buscaría revisar desde que instancia EC2 se están obteniendo las respuestas con error, para descartar que el problema este en una de ellas. Se revisan las metricas de los recursos de cada instancia, esto puede dar contexto al origen del inconveniente.
Por ejemplo, si se obtiene un error 503 (Service Unavailable) puede que el backend en uno de los nodos no este logrando ser accedido via puerto 80/443 etc. Tambien puede que que la instancia este fallando el health check configurado y por ende este inaccesible.
Si los errores son de Gateway timeout, una de las instancias puede estar saturada y por ende tarda tanto en responder que el limite de timeout del balanceador es activado.
En caso de que se obtengan respuestas con error sin importar el nodo, habría que revisar los logs en las instancias o en Cloudwatch, para saber con detalle si puede que las queries a db estén tardando por falta de recursos etc.

Q. **Tenemos un proceso manual que estamos corriendo todas las semanas. Qué harías con este tipo de procesos ? Por qué ?**

A. Tratar de automatizarla.
Primero medir el tiempo que se utiliza en ejecutarse el proceso de forma manual. Luego evaluar cuanto tiempo me costaría automatizarlo y que valor agregado tendría el hacerlo, si la suma del tiempo de ejecución manual a lo largo de un año es menor que lo que me tomaría automatizarlo, probablemente no valga la pena. Hay tareas que de estar automatizadas impactan directamente en la reducción del gasto de un área, por ejemplo, en el área de seguros, la emisión de una póliza.

Q. **Investigando, encontrás una solución que es mucho mejor que la implementada en la empresa. ¿La implementarías? ¿Por qué? ¿Si es muy distinto a lo que la empresa a está acostumbrada, brindarías las capacitaciones y el soporte para hacer una migración ?**

A. Siempre es conveniente planificar la revisión de las metodologías y stack tecnológico usado. En caso de encontrar una herramienta X que provea beneficios demostrados al integrarse a los procesos de la empresa, habría que evaluar el tiempo y costo que amerita la implementación y la capacidad de mantenimiento que el equipo de tecnología puede darle una vez capacitados.

Lo ideal es apuntar a la mejora continua, automatizar e implementar herramientas que provean robustez y agilidad a los procesos internos. Por naturaleza me considero una persona con curiosidad en cuanto a nuevas tecnologías y la forma en que los flujos de trabajo se pueden agilizar con automatización, si tengo la oportunidad de aprender una nueva solución y transmitir a mis compañeros el conocimiento para aprovecharla, la tomo.

Q. **La plataforma está recibiendo un DDOS (Ataque de denegación de servicio distribuido). ¿ Que acciones tomarías para mitigar dicho ataque?**

A. Trataría de ver las métricas de la cantidad de trafico recibido para saber si se pueden levantar instancias adicionales de forma manual en caso de que no se haya planificado autoscaling. Por lo general el bloqueo directo de trafico es ineficiente y hace mas daño de lo que soluciona dado que la cantidad de IPs que realizan el ataque son muchas.

Ver las instancias afectadas basados en el endpoint al que le estén haciendo HIT. Por ejemplo, es común recibir ataques con peticiones POST, por lo que no solo se satura la capa de networking, sino que se eleva la utilización de recursos de las instancias al intentar procesar los datos recibidos. Por defecto AWS provee Shield, que protege contra ataques relativamente pequeños y monitoreo básico. Se puede activar el high tier de Shield para obtener la protección adicional y demás perks pero la verdadera protección ante DDOS es la correcta planificación de la infraestructura y el uso de servicios como CloudFlare.

Una vez recibido el ataque, si no se tiene buena preparación, se activan los servicios como Shield etc, se monitorea el flujo de tráfico para saber si por ejemplo se puede intentar bloquear un rango de IPs de un país en especifico por un breve periodo de tiempo.

Es importante PROBAR la protección, no basta con activar los servicios y esperar que no vuelva a ocurrir. Si es posible contratar algún servicio de stress testing, se puede hacer tuning a nivel de infraestructura para prevenir futuro downtime por ataques.

Q. **¿Que elementos tomarías en cuenta para poder realizar una reducción de costo en la infraestructura?**

A. Usar spot instances y escalar la infraestructura en base a metricas históricas de uso (Cloudwatch). 

Por ejemplo, en vez de tener una instancia C Large activa durante todo el mes dado que en X semana se espera un incremento del tráfico, se puede configurar para que una instancia de menor tamaño escale una vez cumplidas las condiciones. Los autoescaling groups son una de las mejores herramientas en el ecosistema de AWS.

Usar los datos históricos de estas metricas tambien permite analizar cuantos recursos están siendo utilizados por una instancia realmente.

Revisaria que instancias no son necesarias fuera de hora de oficina y schedulearia el uptime solo para horas de trabajo.

Habría que mantener un plan de limpieza de snapshots, AMI, y storages S3 que permita reducir los costos que estos recursos generan por almacenaje.

Si existen  aplicaciones que se pueden pasar a contenedores es un plus, dado que se puede reducir el footprint y al mismo tiempo agilizar los procesos involucrados.

En caso de que se estén ejecutando pipelines desde Gitlab, en algunos casos es posible usar shared runners para no incurrir en el costo de tener una instancia para ello, todo depende del proceso y lo sensible de la data.

Q. **Diagramar lo que consideres una arquitectura de alta disponibilidad. ¿Qué cosas tendrías en cuenta para hacer que suceda lo anterior? en qué casos usarías servicios administrador provenientes del cloud (ej: Amazon RDS, Amazon S3) aunque esto aumente el presupuesto que usando servicios sin administración del cloud?**

A.1 Diagrama

![alt text](img/highavailable.png "High Available Infrastructure")


A.2 Usaría servicios como RDS, S3 y demás, aunque se incrementen los costos si esto impacta positivamente en el rendimiento de la aplicación y da flexibilidad de cara a futuras implementaciones.

Por ejemplo, los costos de usar S3 para respaldar data pueden salirse del presupuesto al considerar la transferencia de datos y precio por GB dependiendo del dataset, pero provee bondades como facilidad de replicación e integridad de datos frente a soluciones on-premise donde se tiene que evaluar la garantía física del equipo, licencias y demás.

...