FROM python:3.8-alpine
LABEL maintainer="Luis Leon <luis.alc08@gmail.com>"

ADD . /application
WORKDIR /application
RUN set -e; \
	apk add --no-cache --virtual .build-deps \
		gcc \
		libc-dev \
		linux-headers \
	; \
	pip install -r src/requirements.txt; \
	apk del .build-deps;
EXPOSE 5000
VOLUME /application

# Start gunicorn
CMD ["gunicorn"  , "-b", "0.0.0.0:5000", "wsgi:app"]
